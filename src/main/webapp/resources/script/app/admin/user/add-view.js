var UserAdd = function () {

    var runUserLevelSelect = function () {
        $("#userLevel").select2({
            allowClear: true,
            placeholder: "Select a Level",

        });   
    };
	
	var getSelectedUserLevel = function(){
		 $("#userLevel").change(function () {
            setUserLevelElements(getUserLevel());
	     });
	};

    var initValidator = function () {
    	
        var form = $('#userProfileForm');
        var errorHandler = $('.errorHandler', form);
        var successHandler = $('.successHandler', form);
        form.validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
            	if (element.attr("type") == "radio" || element.attr("type") == "checkbox" ) { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") == "dd" || element.attr("name") == "mm" || element.attr("name") == "yyyy" || element.closest('.input-group').has('.input-group-btn').length || element.closest('.form-group').has('.input-group-addon').length) {
                    error.insertAfter($(element).closest('.form-group').children('div'));
                } else if (element.closest('.form-group').has('.select2').length ){
                	error.insertAfter($(element).closest('.form-group').children('span'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },

            ignore: "",

            rules: {
            	fullName: {
            		minlength: 2,
            		required: true
            	},
            	userLevel: {
            		required: true,
            	},
                emailAddress: {
                    minlength: 2,
                    required: true,
                    email: true
                },
                userName: {
                	required: true,
                    minlength: 6,
                },
                password: {
            		required: true,
                    minlength: 6,
            	},
            	passwordAgain :{                
            		required: true,
            		minlength: 6,
            		equalTo: password,
            	}
            },

            messages: {
                fullName: "Please specify a Name",
                userLevel: "Please select User Level",
                emailAddress: {
                    email: "Your email address must be in the format of name@domain.com",
                    required: "Please specify a vaild email"
                },
                userName :"Please Specify the User Name",
                password :{
                	required: "Password does not specify",
                	minlength: "Please enter at least {0} characters.",
                },
                passwordAgain : {
                	required: "Re-enter password does not specify",
                	minlength: "Please enter at least {0} characters.",
                	equalTo: "Password does not match",
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                successHandler.hide();
                errorHandler.show();
            },

            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },

            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },

            submitHandler: function (form) {
                successHandler.show();
                errorHandler.hide();
                // submit form
                return true;
            }
        });
    };
	
    return {
        init: function () {
        	runUserLevelSelect();
            initValidator();
        }
    };

}();