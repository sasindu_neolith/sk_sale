jQuery(document).ready(function () {

    Main.init();
    UserAdd.init();
    
    $('#changePassword').iCheck({
        checkboxClass: 'icheckbox_minimal',
    });
    
	$('#changePassword').on('ifChecked', function () {
		$("#password, #passwordAgain").removeAttr('disabled');
	});
	
    $('#changePassword').on('ifUnchecked', function () {
    	$("#password, #passwordAgain").attr('disabled', 'disabled');
    });
    
    $("#password").attr('disabled', 'disabled');
    $("#passwordAgain").attr('disabled', 'disabled');

});