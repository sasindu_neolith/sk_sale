var VehicleSaleAdd = function () {

	var runDatePicker = function () {
        $('.date-picker').datepicker({
            autoclose: true,
            container: '#picker-container',
            language: 'ja'
        });
    };
	
	var runIsSoldSwitch = function () {
        var state = false;
        if ($("#isSold").attr('checked') == "checked") {
            state = true;
            $('#isSold').val('true');
        } else {
            $('#isSold').val('false');
        }

        $("#isSold").bootstrapSwitch({
            onText: "Sold",
            offText: "Not Sold",
            state: state
        }).on('switchChange.bootstrapSwitch', function (event, state) {
            if ($('#isSold').val() == 'true') {
                $('#isSold').val('false');
            } else if ($('#isSold').val() == 'false') {
                $('#isSold').val('true');
            }
        });
    };

    var initValidator = function () {
    	
        var form = $('#vehicle-sale-form');
        var errorHandler = $('.errorHandler', form);
        var successHandler = $('.successHandler', form);
        form.validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
            	if (element.attr("type") == "radio" || element.attr("type") == "checkbox" ) { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") == "dd" || element.attr("name") == "mm" || element.attr("name") == "yyyy" || element.closest('.input-group').has('.input-group-btn').length || element.closest('.form-group').has('.input-group-addon').length) {
                    error.insertAfter($(element).closest('.form-group').children('div'));
                } else if (element.closest('.form-group').has('.select2').length ){
                	error.insertAfter($(element).closest('.form-group').children('span'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },

            ignore: "",

            rules: {
            	date: {
                    required: true
                },
                supplierName: {
                    required: true
                },
                itemName: {
                    required: true
                },
                chassisNo: {
                    required: true
                },
                paymentDate: {
                    required: true
                },
                purchasedAmount: {
                    required: true,
                    number: true
                },
                taxAmount: {
                	number: true
                },
                totalAmount: {
                	number: true
                },
                sellingAmount: {
                	number: true
                },
                bankDepositAmount: {
                	number: true
                },
                bankCommisionAmount: {
                	number: true
                },
                cashDepositAmount: {
                	number: true
                },
                repairChargeAmount: {
                	number: true
                },
                recycleChargeAmount: {
                	number: true
                }
            },

            messages: {
            	date: "Please Select a Date.",
            	supplierName: "Please Insert Supplier.",
            	itemName: "Please Insert Vehicle / Machine.",
            	chassisNo : "Please Insert Chassis No.",
            	paymentDate : "Please Select a Payment Date.",
            	purchasedAmount : {
            		required : "Please Insert a Perchased amount.",
            		number : "Please insert a numeric value"
            	},
            	taxAmount : "Please insert a numeric value.",
            	totalAmount : "Please insert a numeric value.",
            	sellingAmount : "Please insert a numeric value.",
            	bankDepositAmount : "Please insert a numeric value.",
            	bankCommisionAmount : "Please insert a numeric value.",
            	cashDepositAmount : "Please insert a numeric value.",
            	repairChargeAmount : "Please insert a numeric value.",
            	recycleChargeAmount : "Please insert a numeric value."
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                successHandler.hide();
                errorHandler.show();
            },

            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },

            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },

            submitHandler: function (form) {
                successHandler.show();
                errorHandler.hide();
                // submit form
                return true;
            }
        });
    };
	
    return {
        init: function () {
        	runDatePicker();
        	runIsSoldSwitch();
            initValidator();
        }
    };

}();