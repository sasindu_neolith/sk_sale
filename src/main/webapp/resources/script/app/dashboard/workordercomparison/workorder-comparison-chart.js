var WorkorderComparisonChart = function () {
    //function to initiate jQRangeSlider
    //There are plenty of options you can set to control the precise looks of your plot. 
    //You can control the ticks on the axes, the legend, the graph type, etc.
    //For more information, please visit http://www.flotcharts.org/
    var runCharts = function () {

    	$.ajax({
            type: "GET",
            url: 'restapi/dashboard/workorder-comparison-chart',
            error: function () {
                alert("An error occurred.");
            },
            success: function (data) {  
            	var openWo = getBarGraphData(data.openWorkOrders);
            	var closedWo = getBarGraphData(data.closeWorkOrders);
            	
            	createBarGraph(openWo, closedWo, data.openBarColor, data.closedBarColor);
            }
        }); 
    	
    };
    
    var getBarGraphData = function(data) {
    	var out = [];
    	$.each( data , function( key, value ) {
    		var dataObj = [value.month , value.count];
    		out.push(dataObj);
    	});
    	return out;
	};
    
    var createBarGraph = function(data1, data2, color1, color2) {
    	
    	$.plot("#placeholder3",  [
        	{ 
        		data: data1,
        		color: color1,
	        	label: ' Open',
	            bars: {
	                show: true,
	                barWidth: 0.4,
	                align: "right",
	            }
	        },
	        {
	            data: data2,
	            color: color2,
	            label: ' Close',
	            bars: {
	                show: true,
	                barWidth: 0.4,
	                align: "left", 
	            }
	        }
        	
        ], {
        	xaxis: {
                mode: "categories",  
                tickLength: 1,

            },
            grid: {
                hoverable: true,
            },
            yAxis: {
                allowDecimals: false,
            }
        }); 
	};
	
    return { 
    	
        init: function () {
            runCharts();
        }
    };
}();