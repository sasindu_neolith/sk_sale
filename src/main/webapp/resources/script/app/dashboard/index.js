﻿var Index = function () {    
	
    return {
        init: function () {
        	UpcomingScheduleMaintenance.init();
        	AssetWorkOrderHistory.init();
        	EmployeeHistory.init();
        	WorkorderComparisonChart.init();
        }
    };
}();