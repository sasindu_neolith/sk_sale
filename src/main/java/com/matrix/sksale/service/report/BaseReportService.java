package com.matrix.sksale.service.report;

import java.io.OutputStream;
import java.util.List;

import com.matrix.sksale.dto.BaseReportDTO;
import com.matrix.sksale.dto.BaseReportFilterDTO;

public interface BaseReportService<DTO extends BaseReportDTO, FILTER extends BaseReportFilterDTO> {

    DTO findReport(Integer id);

    List<DTO> findReport(FILTER filter);

    void generatePDFReport(List<DTO> dtoList, String reportDir, String inputFilePath, OutputStream outputStream);

    void generateCSVReport(List<DTO> dtoList, String reportDir, String inputFilePath, OutputStream outputStream);
}
