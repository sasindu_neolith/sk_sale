package com.matrix.sksale.service.userprofile.api;

import org.springframework.web.multipart.MultipartFile;

import com.matrix.sksale.dto.admin.UserDTO;
import com.matrix.sksale.model.admin.User;
import com.matrix.sksale.result.userprofile.UserProfileResult;

public interface UserProfileService {

    UserProfileResult update(UserDTO userDTO, MultipartFile file); 

    User findEntityById(Integer id);
}
