package com.matrix.sksale.service.admin.impl;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataIntegrityViolationException; 
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.matrix.sksale.dao.admin.*;
import com.matrix.sksale.dto.admin.UserDTO;
import com.matrix.sksale.mappers.admin.UserMapper;
import com.matrix.sksale.model.admin.*;
import com.matrix.sksale.params.VelocityMail;
import com.matrix.sksale.repository.FocusDataTablesInput;
import com.matrix.sksale.result.admin.UserResult;
import com.matrix.sksale.service.admin.api.UserService;
import com.matrix.sksale.util.FileDownloadUtil;
import com.matrix.sksale.util.VelocityEmailSender;
import com.matrix.sksale.util.search.admin.UserSearchPropertyMapper;

import javax.persistence.criteria.Predicate;
import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
	
	private final String USER_DEFAULT_IMAGE = "/resources/images/user-default.png";

	@Autowired
	private UserDao userDao;

	@Autowired
	private UserCredentialDao userCredentialDao;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private VelocityEmailSender velocityEmailSender;
	
	@Autowired
	private Environment environment;

	@Override
	public UserResult delete(Integer id) {
		UserResult result = new UserResult(null, null);
		try {
			userDao.delete(id);
			result.setResultStatusSuccess();
			result.addToMessageList("User Deleted Successfully.");
		} catch (DataIntegrityViolationException e) {
			result.setResultStatusError();
			result.addToErrorList("User Already Assigned. Please Remove from Assigned User and try again.");
		} catch (Exception ex) {
			result.setResultStatusError();
			result.addToErrorList(ex.getMessage());
		}
		return result;
	}

	@Override
	public UserResult save(UserDTO dto) throws Exception {
		UserResult result = createUserResult(dto);
		try{
			saveOrUpdate(result);
			result.addToMessageList(getMessageByAction(dto));
		} catch (ObjectOptimisticLockingFailureException e) {
			result.setResultStatusError();
			result.addToErrorList("User Already updated. Please Reload User.");
		} catch (Exception ex) {
			ex.printStackTrace();
			result.setResultStatusError();
			result.addToErrorList(ex.getMessage());
		}

		return result;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	private void saveOrUpdate(UserResult result) throws Exception {
		UserMapper.getInstance().dtoToDomain(result.getDtoEntity(), result.getDomainEntity());
		setUserData(result);
		userDao.save(result.getDomainEntity());
		result.updateDtoIdAndVersion();
	}

	private UserResult createUserResult(UserDTO dto) {
		UserResult result;
		if ((dto.getId() != null) && (dto.getId() > 0)) {
			result = new UserResult(userDao.findOne(dto.getId()), dto);
		} else {
			result = new UserResult(new User(), dto);
		}

		return result;
	}

	private String getMessageByAction(UserDTO dto) {
		if (dto.getId() == null) {
			return "User Added Successfully.";
		} else {
			return "User Updated Successfully.";
		}
	}

	private void setUserData(UserResult result) throws Exception {
		setUserCredentials(result);
	}

	private void setUserCredentials(UserResult result) {
		UserCredential userCredential = new UserCredential();
		if ((result.getDtoEntity().getId() != null) && (result.getDtoEntity().getId() > 0)) {
			userCredential = userCredentialDao.findOne(result.getDtoEntity().getUserCredential().getId());
			if (result.getDtoEntity().getChangePassword() == true) {
				userCredential.setPassword(passwordEncoder.encode(result.getDtoEntity().getUserCredential().getPassword()));
				sendEmailToUser(result);
			}
			userCredential.setUserName(result.getDtoEntity().getUserCredential().getUserName());
		} else {
			userCredential = new UserCredential();
			setUserName(userCredential, result);
			setPassword(userCredential, result);
			userCredential.setIsDeleted(Boolean.FALSE);
			sendEmailToUser(result);
		}
		userCredential.setUser(result.getDomainEntity());
		result.getDomainEntity().setUserCredentials(userCredential);
	}

	private void setUserName(UserCredential userCredential, UserResult result) {
		if ( result.getDtoEntity().getUserCredential().getUserName() != null ) {
			userCredential.setUserName(result.getDtoEntity().getUserCredential().getUserName());
		} else {
			userCredential.setUserName(getSystemUserName(result.getDtoEntity().getFullName()));			
		}
	}

	private void setPassword(UserCredential userCredential, UserResult result) {
		if (result.getDtoEntity().getUserCredential().getPassword() != null) {
			userCredential.setPassword(passwordEncoder.encode(result.getDtoEntity().getUserCredential().getPassword()));
		} else {
			userCredential.setPassword(passwordEncoder.encode(getSystemPassword())); 
		}
	}

	private String getSystemUserName(String fullName) {
		String username[] = fullName.trim().split("\\s+");
		return username[0].toLowerCase();
	}

	@Override
	public String getSystemPassword() {
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@#$&";
		return RandomStringUtils.random(6, characters);
	}

	private void sendEmailToUser(UserResult result) {
		try {
			VelocityMail velocityMail = new VelocityMail();
			velocityMail.getModel().put("user", result.getDtoEntity().getFullName());
			velocityMail.getModel().put("password", result.getDtoEntity().getUserCredential().getPassword());
			velocityMail.getModel().put("userName", result.getDtoEntity().getUserCredential().getUserName());
			velocityMail.setSubject("Login Information");
			velocityMail.setTo(result.getDtoEntity().getEmailAddress());
			velocityMail.setVmTemplate("usercredentials");
			velocityEmailSender.sendEmail(velocityMail);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public UserDTO findById(Integer id) throws Exception {
		User domain = userDao.findOne(id);
		if (domain != null) {
			UserDTO dto = UserMapper.getInstance().domainToDto(domain);
			return dto;
		}
		return null;
	}

	@Override
	public UserDTO findUserById(Integer userId) throws Exception {
		User user = findEntityById(userId);
		UserDTO dto = UserMapper.getInstance().domainToDto(user);

		return dto;
	}

	@Override
	public DataTablesOutput<UserDTO> findAll(FocusDataTablesInput input) throws Exception {
		DataTablesOutput<User> domainOut;
		
		UserSearchPropertyMapper.getInstance().generateFocusDataTableInput(input);
		
		domainOut = userDao.findAll(input);

		DataTablesOutput<UserDTO> out = UserMapper.getInstance().domainToDTODataTablesOutput(domainOut);

		return out;
	}

	@Override
	public UserDTO findUser(UserDTO dto) {
		try {
			return UserMapper.getInstance().domainToDto(userDao.findOne(dto.getId()));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<UserDTO> findUserList() {
		try {
			return UserMapper.getInstance().domainToDTOList(userDao.findAll());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public User findByEmail(String email) throws Exception {
		User user = userDao.findByEmail(email);
		return user;
	}

	@Override
	public User findEntityById(Integer id) {
		User user = userDao.findOne(id);
		return user;
	}

	@Override
	public DataTablesOutput<UserDTO> findAllByBusiness(FocusDataTablesInput input, Integer id) {
		try {
			DataTablesOutput<User> domainOut = userDao.findAll(input, findAllUserByBusinessSpec(id));
			return UserMapper.getInstance().domainToDTODataTablesOutput(domainOut);
		} catch (Exception e) {
			e.printStackTrace();
			return new DataTablesOutput<>();
		}
	}

	private Specification<User> findAllUserByBusinessSpec(Integer id) {
		Specification<User> spec = (root, query, cb) -> {
			List<Predicate> predicates = new ArrayList<>();
            predicates.add(cb.equal(root.get("business").get("id"), id));
            return cb.and(predicates.toArray(new Predicate[0]));
		};
		
		return spec;
	}

	@Override
	public List<UserDTO> findAll() {
		List<User> userList = (List<User>) userDao.findAll();
		try {
			return UserMapper.getInstance().domainToDTOList(userList);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public byte[] getUserAvatar(Integer id, HttpServletRequest request) throws IOException { 

		if (id != null) {
			String imagePath = userDao.getUserAvatarPath(id);
			String uploadLocation = new File(environment.getProperty("upload.location")).getPath();
			if (imagePath != null) {
				return FileDownloadUtil.getByteInputStream( uploadLocation + imagePath );
			}
		} 
		
		return FileDownloadUtil.getByteInputStream( request.getServletContext().getRealPath("").concat(USER_DEFAULT_IMAGE) );
	}
}
