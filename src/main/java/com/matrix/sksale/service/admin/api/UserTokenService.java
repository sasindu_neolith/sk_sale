package com.matrix.sksale.service.admin.api;

import com.matrix.sksale.dto.admin.UserTokenDTO;

public interface UserTokenService {

	UserTokenDTO findById(Integer id) throws Exception;

	UserTokenDTO findbytoken(String token) throws Exception;

	void delete(Integer id);

	Integer upate(UserTokenDTO userTokenDTO) throws Exception;

	Integer save(UserTokenDTO tokenDTO) throws Exception;

}
