package com.matrix.sksale.service.login.api;

import org.springframework.ui.Model;

import com.matrix.sksale.dto.admin.UserCredentialDTO;
import com.matrix.sksale.result.LoginResult;

public interface LoginService {

    LoginResult sendResetEmail(String email);

    LoginResult resetPassword(String token, Model modal);

    LoginResult updatePassword(UserCredentialDTO credentialDTO);

}
