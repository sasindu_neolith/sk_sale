package com.matrix.sksale.service.sale.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.matrix.sksale.dao.sale.VehicleSaleDao;
import com.matrix.sksale.dto.sale.VehicleSaleDTO;
import com.matrix.sksale.mappers.sale.VehicleSaleMapper;
import com.matrix.sksale.model.sale.VehicleSale;
import com.matrix.sksale.repository.FocusDataTablesInput;
import com.matrix.sksale.result.sale.VehicleSaleResult;
import com.matrix.sksale.service.sale.api.VehicleSaleService;
import com.matrix.sksale.util.search.sale.VehicleSaleSearchPropertyMapper;

@Service
public class VehicleSaleServiceImpl implements VehicleSaleService {
	
	private final static Logger logger = LoggerFactory.getLogger(VehicleSaleServiceImpl.class);
	
	@Autowired
	private VehicleSaleDao vehicleSaleDao;

	@Override
	public VehicleSaleDTO findById(Integer id) throws Exception {
		VehicleSale domain = vehicleSaleDao.findOne(id);
		if (domain != null) {
			return VehicleSaleMapper.getInstance().domainToDto(domain);
		}
		return null;
	}

	@Override
	public VehicleSaleResult save(VehicleSaleDTO vehicleSale) {
	
		VehicleSaleResult result = createVehicleSaleResult(vehicleSale);
		try {
			saveOrUpdate(result);
		} catch (ObjectOptimisticLockingFailureException e) {
			result.setResultStatusError();
			result.addToErrorList("Sale Already updated. Please Reload Sale.");
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex.getMessage());
			result.setResultStatusError();
			result.addToErrorList(ex.getMessage());
		}
	
		return result;
	}
	
	private VehicleSaleResult createVehicleSaleResult(VehicleSaleDTO dto) {
		VehicleSaleResult result;
		if ((dto.getId() != null) && (dto.getId() > 0)) {
			result = new VehicleSaleResult(vehicleSaleDao.findOne(dto.getId()), dto);
		} else {
			result = new VehicleSaleResult(new VehicleSale(), dto);
		}
	
		return result;
	}
	
	private String getMessageByAction(VehicleSaleResult result) {
		if (result.getDtoEntity().getId() == null) {
			return "Sale Added Successfully.";
		} else {
			return "Sale Updated Successfully.";
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	private void saveOrUpdate(VehicleSaleResult result) throws Exception {
		VehicleSaleMapper.getInstance().dtoToDomain(result.getDtoEntity(), result.getDomainEntity());
		setVehicleSaleData(result);
		vehicleSaleDao.save(result.getDomainEntity());
		result.addToMessageList(getMessageByAction(result));
		result.updateDtoIdAndVersion();
	}
	
	private void setVehicleSaleData(VehicleSaleResult result) throws Exception {
		
	}

	@Override
	public VehicleSaleResult delete(Integer id) {
		VehicleSaleResult result = new VehicleSaleResult(null, null);
		try {
			vehicleSaleDao.delete(id);
			result.setResultStatusSuccess();
			result.addToMessageList("Sale Deleted Successfully.");
		} catch (DataIntegrityViolationException e) {
			result.setResultStatusError();
			result.addToErrorList("Sale Already Assigned. Please Remove from Assigned items and try again.");
		} catch (Exception ex) {
			result.setResultStatusError();
			result.addToErrorList(ex.getMessage());
		}
		return result;
	}

	@Override
	public DataTablesOutput<VehicleSaleDTO> findAll(FocusDataTablesInput input) throws Exception {
		VehicleSaleSearchPropertyMapper.getInstance().generateFocusDataTableInput(input);
		DataTablesOutput<VehicleSale> domainOut = vehicleSaleDao.findAll(input);
		final DataTablesOutput<VehicleSaleDTO> out = VehicleSaleMapper.getInstance().domainToDTODataTablesOutput(domainOut);
		return out;
	}

}