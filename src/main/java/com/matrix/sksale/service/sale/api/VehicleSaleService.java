package com.matrix.sksale.service.sale.api;

import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import com.matrix.sksale.dto.sale.VehicleSaleDTO;
import com.matrix.sksale.repository.FocusDataTablesInput;
import com.matrix.sksale.result.sale.VehicleSaleResult;

public interface VehicleSaleService {

	VehicleSaleDTO findById(Integer id) throws Exception;

	VehicleSaleResult save(VehicleSaleDTO vehicleSale);

	VehicleSaleResult delete(Integer id);

	DataTablesOutput<VehicleSaleDTO> findAll(FocusDataTablesInput input) throws Exception;

}
