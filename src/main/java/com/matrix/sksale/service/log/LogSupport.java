package com.matrix.sksale.service.log;

import com.matrix.sksale.model.BaseModel;

public interface LogSupport {

	void createPersistLog(BaseModel model, String notes);
	
	void createUpdateLog(BaseModel model, String notes);
	
	void createRemoveLog(BaseModel model, String notes);
	
}
