package com.matrix.sksale.mappers.sale;

import com.matrix.sksale.dto.sale.VehicleSaleDTO;
import com.matrix.sksale.mappers.GenericMapper;
import com.matrix.sksale.model.sale.VehicleSale;
import com.matrix.sksale.util.DateUtil;

public class VehicleSaleMapper extends GenericMapper<VehicleSale, VehicleSaleDTO> {
	
	private static VehicleSaleMapper instance = null;

	private VehicleSaleMapper(){
	}

	public static VehicleSaleMapper getInstance() {
		if (instance == null) {
			instance = new VehicleSaleMapper();
		}
		return instance;
	}

	@Override
	public VehicleSaleDTO domainToDto(VehicleSale domain) throws Exception {
		VehicleSaleDTO dto = new VehicleSaleDTO();
		dto.setId(domain.getId());
		dto.setDate(domain.getDate());
		dto.setSupplierName(domain.getSupplierName());
		dto.setItemName(domain.getItemName());
		dto.setChassisNo(domain.getChassisNo());
		dto.setPaymentDate(domain.getPaymentDate());
		dto.setPurchasedAmount(domain.getPurchasedAmount());
		dto.setTaxAmount(domain.getTaxAmount());
		dto.setTotalAmount(domain.getTotalAmount());
		dto.setCustomerName(domain.getCustomerName());
		dto.setSellingDate(domain.getSellingDate());
		dto.setSellingAmount(domain.getSellingAmount());
		dto.setBankDepositDate(domain.getBankDepositDate());
		dto.setBankDepositAmount(domain.getBankDepositAmount());
		dto.setBankCommisionAmount(domain.getBankCommisionAmount());
		dto.setCashDepositDate(domain.getCashDepositDate());
		dto.setCashDepositAmount(domain.getCashDepositAmount());
		dto.setRepairDate(domain.getRepairDate());
		dto.setRepairChargeAmount(domain.getRepairChargeAmount());
		dto.setRecycleChargeAmount(domain.getRecycleChargeAmount());
		dto.setIsSold(domain.getIsSold());
		
		setCommanDTOFields(dto, domain);
		
		return dto;
	}

	@Override
	public void dtoToDomain(VehicleSaleDTO dto, VehicleSale domain) throws Exception {
		domain.setId(dto.getId());
		domain.setDate(dto.getDate());
		domain.setSupplierName(dto.getSupplierName());
		domain.setItemName(dto.getItemName());
		domain.setChassisNo(dto.getChassisNo());
		domain.setPaymentDate(dto.getPaymentDate());
		domain.setPurchasedAmount(dto.getPurchasedAmount());
		domain.setTaxAmount(dto.getTaxAmount());
		domain.setTotalAmount(dto.getTotalAmount());
		domain.setCustomerName(dto.getCustomerName());
		domain.setSellingDate(dto.getSellingDate());
		domain.setSellingAmount(dto.getSellingAmount());
		domain.setBankDepositDate(dto.getBankDepositDate());
		domain.setBankDepositAmount(dto.getBankDepositAmount());
		domain.setBankCommisionAmount(dto.getBankCommisionAmount());
		domain.setCashDepositDate(dto.getCashDepositDate());
		domain.setCashDepositAmount(dto.getCashDepositAmount());
		domain.setRepairDate(dto.getRepairDate());
		domain.setRepairChargeAmount(dto.getRepairChargeAmount());
		domain.setRecycleChargeAmount(dto.getRecycleChargeAmount());
		domain.setIsSold(domain.getIsSold());

		setCommanDomainFields(dto, domain);
	}

	@Override
	public VehicleSaleDTO domainToDtoForDataTable(VehicleSale domain) throws Exception {
		VehicleSaleDTO dto = new VehicleSaleDTO();
		dto.setId(domain.getId());
		dto.setStrDate(DateUtil.getSimpleDateString(domain.getDate()));
		dto.setSupplierName(domain.getSupplierName());
		dto.setItemName(domain.getItemName());
		dto.setCustomerName(domain.getCustomerName());
		
        return dto;
	}

}
