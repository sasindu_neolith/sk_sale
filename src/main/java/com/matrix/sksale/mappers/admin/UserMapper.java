package com.matrix.sksale.mappers.admin;

import com.matrix.sksale.dto.admin.UserDTO;
import com.matrix.sksale.mappers.GenericMapper;
import com.matrix.sksale.model.admin.User;

public class UserMapper extends GenericMapper<User, UserDTO> {

	private static UserMapper instance = null;

	private UserMapper(){
	}

	public static UserMapper getInstance() {
		if (instance == null) {
			instance = new UserMapper();
		}
		return instance;
	}

	@Override
	public UserDTO domainToDto(User domain) throws Exception {
		UserDTO dto = new UserDTO();
		domainToDto(domain,dto);
		return dto;
	}

	public UserDTO domainToViewDto(User domain) throws Exception {
		UserDTO dto = new UserDTO();
		setBasicUserData(domain, dto);
		return dto;
	}

	private void domainToDto(User domain, UserDTO dto) throws Exception {
		dto.setImagePath(domain.getImagePath());
		dto.setIsActive(domain.getActive());
		dto.setUserCredential(UserCredentialMapper.getInstance().domainToDto(domain.getUserCredential())); 
		setBasicUserData(domain, dto);

		setCommanDTOFields(dto, domain);
	}

	private void setBasicUserData(User domain, UserDTO dto) {
		dto.setId(domain.getId());
		dto.setFullName(domain.getFullName());
		dto.setAddress(domain.getAddress());
		dto.setEmailAddress(domain.getEmailAddress());
		dto.setPersonalCode(domain.getPersonalCode());
		dto.setNotes(domain.getNotes());
		dto.setTelephone1(domain.getTelephone1());
		dto.setTelephone2(domain.getTelephone2());
		dto.setUserTitle(domain.getUserTitle());
	}

	@Override
	public void dtoToDomain(UserDTO dto,User domain) throws Exception {

		domain.setId(dto.getId());
		domain.setVersion(dto.getVersion());
		domain.setFullName(dto.getFullName());
		domain.setAddress(dto.getAddress());
		domain.setEmailAddress(dto.getEmailAddress());
		domain.setPersonalCode(dto.getPersonalCode());
		domain.setNotes(dto.getNotes());
		domain.setTelephone1(dto.getTelephone1());
		domain.setTelephone2(dto.getTelephone2());
		domain.setUserTitle(dto.getUserTitle());

		domain.setActive(dto.getIsActive());

		setCommanDomainFields(dto, domain);
	}

	public void dtoToBasicDomain(UserDTO dto, User domain) throws Exception {

		domain.setId(dto.getId());
		domain.setVersion(dto.getVersion());
		domain.setFullName(dto.getFullName());
		domain.setAddress(dto.getAddress());
		domain.setEmailAddress(dto.getEmailAddress());
		domain.setTelephone1(dto.getTelephone1());
		domain.setTelephone2(dto.getTelephone2());
		domain.setUserTitle(dto.getUserTitle());
		domain.setImagePath(dto.getImagePath());
	}

	@Override
	public UserDTO domainToDtoForDataTable(User domain) throws Exception {
		UserDTO dto = new UserDTO();
		dto.setId(domain.getId());
		dto.setFullName(domain.getFullName());
		dto.setEmailAddress(domain.getEmailAddress());
		dto.setPersonalCode(domain.getPersonalCode());
		
        return dto;
	}
}