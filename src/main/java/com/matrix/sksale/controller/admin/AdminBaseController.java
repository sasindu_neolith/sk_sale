package com.matrix.sksale.controller.admin;
 

import com.matrix.sksale.controller.BaseController;

public abstract class AdminBaseController extends BaseController {


	public String getLevelTwo(){
		return getLevelOne().concat("/Setting");
	}
	
	public String getLevelTwoNotification(){
		return getLevelOne().concat("/Notification");
	}


}
