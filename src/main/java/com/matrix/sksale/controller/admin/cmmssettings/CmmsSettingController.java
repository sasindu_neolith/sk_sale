package com.matrix.sksale.controller.admin.cmmssettings;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(CmmsSettingController.REQUEST_MAPPING_URL)
public class CmmsSettingController {

	public static final String REQUEST_MAPPING_URL = "/cmmssettings";

}
