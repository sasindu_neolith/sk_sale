package com.matrix.sksale.controller.admin.user; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.matrix.sksale.dto.admin.UserDTO;
import com.matrix.sksale.repository.FocusDataTablesInput;
import com.matrix.sksale.service.admin.api.UserService;
import com.matrix.sksale.util.FileDownloadUtil;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List; 

@RestController
@RequestMapping(UserRestController.REQUEST_MAPPING_URL)
public class UserRestController {

	public static final String REQUEST_MAPPING_URL = "restapi/users";

	private final String USER_DEFAULT_IMAGE = "/resources/images/user-default.png";

	@Autowired
	private UserService userService;


	@RequestMapping(value = "/userlist", method = RequestMethod.GET)
	public List<UserDTO> getUserList() {
		try {
			return userService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

    @RequestMapping(value = "/getuserlist", method = {RequestMethod.GET})
    public DataTablesOutput<UserDTO> getUserListTableData(@Valid FocusDataTablesInput input) throws Exception {
        return userService.findAll(input);
    }

    
    @RequestMapping(value = "/my-avatar", method = {RequestMethod.GET})
    public @ResponseBody byte[] getUserAvatar(Integer id, HttpServletRequest request) throws IOException {
		try {
			return userService.getUserAvatar(id, request);
		} catch (Exception e) {
			return FileDownloadUtil.getByteInputStream( request.getServletContext().getRealPath("").concat(USER_DEFAULT_IMAGE));
		} 
    }

}
