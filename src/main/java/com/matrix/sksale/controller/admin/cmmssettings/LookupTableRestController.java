package com.matrix.sksale.controller.admin.cmmssettings;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(LookupTableRestController.REQUEST_MAPPING_URL)
public class LookupTableRestController {

	public static final String REQUEST_MAPPING_URL = "restapi/lookuptable";

}
