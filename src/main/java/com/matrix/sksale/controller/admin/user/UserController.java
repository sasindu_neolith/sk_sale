package com.matrix.sksale.controller.admin.user;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.matrix.sksale.constants.ResultStatus;
import com.matrix.sksale.controller.admin.AdminBaseController;
import com.matrix.sksale.dto.admin.UserDTO;
import com.matrix.sksale.result.admin.UserResult;
import com.matrix.sksale.service.admin.api.UserService;
import com.matrix.sksale.constants.UserLevel;


@Controller
@RequestMapping(UserController.REQUEST_MAPPING_URL)
public class UserController extends AdminBaseController {

	public static final String REQUEST_MAPPING_URL = "/userProfile";

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model) {
		return "admin/user/home-view";
	}

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index(Model model, @ModelAttribute("success") final ArrayList<String> success, @ModelAttribute("error") final ArrayList<String> error) {
		model.addAttribute("success", success);
		model.addAttribute("error", error);

		return "admin/user/home-view";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String add(Model model, RedirectAttributes ra) throws Exception {
		try {
			setCommonData(model, new UserDTO());
			return "admin/user/add-view";
		} catch (Exception ex) {
			ra.addFlashAttribute("error", new ArrayList<String>().add("Error While Loading Initial Data."));
			return "redirect:/userProfile/index";
		}
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String edit(Integer id, Model model, RedirectAttributes ra) {
		try {
			UserDTO user = userService.findById(id);
			setCommonData(model, user);
			return "admin/user/add-view";
		} catch (Exception e) {
			ra.addFlashAttribute("error", new ArrayList<String>().add("Error occured. Please Try again."));
			return "redirect:/userProfile/index";
		}
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveOrUpdate(@ModelAttribute("user") @Valid UserDTO user, Model model) throws Exception {

		UserResult result = userService.save(user);

		if (result.getStatus().equals(ResultStatus.ERROR)) {
			model.addAttribute("error", result.getErrorList());
		} else {
			user = userService.findById(result.getDomainEntity().getId());
			model.addAttribute("success", result.getMsgList());
		}

		setCommonData(model, user);
		return "admin/user/add-view";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String delete(Integer id, Model model, RedirectAttributes ra) {

		UserResult result = userService.delete(id);

		if (result.getStatus().equals(ResultStatus.ERROR)) {
			ra.addFlashAttribute("error", result.getErrorList());
		} else {
			ra.addFlashAttribute("success", result.getMsgList());
		}

		return "redirect:/userProfile/index";
	}

	@RequestMapping(value = "/users", method = {RequestMethod.GET})
	private String getUserList(Model model) {
		try {
			model.addAttribute("users", userService.findAll());
		} catch (Exception ex) {
			ex.printStackTrace();
			model.addAttribute("users", new ArrayList<UserDTO>());
		}

		return "admin/user/home-view";
	}

	private void setCommonData(Model model, UserDTO user) throws Exception {
		model.addAttribute("user", user);
		model.addAttribute("userLevels", UserLevel.getUserLevels());
	}

}
