package com.matrix.sksale.controller.sale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.matrix.sksale.dto.sale.VehicleSaleDTO;
import com.matrix.sksale.repository.FocusDataTablesInput;
import com.matrix.sksale.service.sale.api.VehicleSaleService;

@RestController
@RequestMapping(VehicleSaleRestController.REQUEST_MAPPING_URL)
public class VehicleSaleRestController {
	
	public static final String REQUEST_MAPPING_URL = "restapi/vehicle-sale";
	
	@Autowired
	private VehicleSaleService vehicleSaleService;
	
	@RequestMapping(value = "/tabledata", method = RequestMethod.GET)
    public DataTablesOutput<VehicleSaleDTO> getAssetList(@Valid FocusDataTablesInput input) {
    	try {
    		return vehicleSaleService.findAll(input);
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return null;
    }

}
