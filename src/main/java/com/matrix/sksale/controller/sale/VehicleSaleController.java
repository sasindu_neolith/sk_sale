package com.matrix.sksale.controller.sale;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.matrix.sksale.constants.ResultStatus;
import com.matrix.sksale.constants.UserLevel;
import com.matrix.sksale.dto.sale.VehicleSaleDTO;
import com.matrix.sksale.result.sale.VehicleSaleResult;
import com.matrix.sksale.service.sale.api.VehicleSaleService;

@Controller
@RequestMapping(VehicleSaleController.REQUEST_MAPPING_URL)
public class VehicleSaleController {
	
	public static final String REQUEST_MAPPING_URL = "/vehicle-sale";
	
	@Autowired
	private VehicleSaleService vehicleSaleService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model) {
		return "sale/vehicle/home-view";
	}

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index(Model model, @ModelAttribute("success") final ArrayList<String> success, @ModelAttribute("error") final ArrayList<String> error) {
		model.addAttribute("success", success);
		model.addAttribute("error", error);

		return "sale/vehicle/home-view";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String add(Model model, RedirectAttributes ra) throws Exception {
		try {
			setCommonData(model, new VehicleSaleDTO());
			return "sale/vehicle/add-view";
		} catch (Exception ex) {
			ra.addFlashAttribute("error", new ArrayList<String>().add("Error While Loading Initial Data."));
			return "redirect:/vehicle-sale/index";
		}
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String edit(Integer id, Model model, RedirectAttributes ra) {
		try {
			VehicleSaleDTO vehicleSaleDTO = vehicleSaleService.findById(id);
			setCommonData(model, vehicleSaleDTO);
			
			return "sale/vehicle/add-view";
		} catch (Exception e) {
			ra.addFlashAttribute("error", new ArrayList<String>().add("Error occured. Please Try again."));
			return "redirect:/vehicle-sale/index";
		}
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveOrUpdate(@ModelAttribute("vehicleSale") @Valid VehicleSaleDTO vehicleSale, Model model) throws Exception {

		VehicleSaleResult result = vehicleSaleService.save(vehicleSale);

		if (result.getStatus().equals(ResultStatus.ERROR)) {
			model.addAttribute("error", result.getErrorList());
		} else {
			vehicleSale = vehicleSaleService.findById(result.getDomainEntity().getId());
			model.addAttribute("success", result.getMsgList());
		}
		setCommonData(model, vehicleSale);
		
		return "sale/vehicle/add-view";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String delete(Integer id, Model model, RedirectAttributes ra) {

		VehicleSaleResult result = vehicleSaleService.delete(id);

		if (result.getStatus().equals(ResultStatus.ERROR)) {
			ra.addFlashAttribute("error", result.getErrorList());
		} else {
			ra.addFlashAttribute("success", result.getMsgList());
		}

		return "redirect:/vehicle-sale/index";
	}

	private void setCommonData(Model model, VehicleSaleDTO vehicleSale) throws Exception {
		model.addAttribute("vehicleSale", vehicleSale);
		model.addAttribute("userLevels", UserLevel.getUserLevels());
	}

}
