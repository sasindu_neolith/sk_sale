package com.matrix.sksale.controller.dashboard;
 
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(DashBoardController.REQUEST_MAPPING_URL)
public class DashBoardController {
	
	public static final String REQUEST_MAPPING_URL = "/dashboard";
    
}
