package com.matrix.sksale.controller.userprofile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.matrix.sksale.constants.ResultStatus;
import com.matrix.sksale.dto.admin.UserDTO;
import com.matrix.sksale.result.userprofile.UserProfileResult;
import com.matrix.sksale.service.admin.api.UserService;
import com.matrix.sksale.service.userprofile.api.UserProfileService;
import com.matrix.sksale.util.AuthenticationUtil;

import javax.validation.Valid;
import java.util.ArrayList;

@Controller
@RequestMapping(UserProfileController.REQUEST_MAPPING_URL)
@PropertySource(value = {"classpath:application.properties"})
public class UserProfileController {

	public static final String REQUEST_MAPPING_URL = "/profile";

	@Autowired
	private UserService userService;

	@Autowired
    private UserProfileService userProfileService;

	@Value("${upload.location}")
	private String uploadLocation;

	@RequestMapping(value = "/myaccount", method = RequestMethod.GET)
    public String index(Model model, @ModelAttribute("success") final ArrayList<String> success, @ModelAttribute("error") final ArrayList<String> error, @ModelAttribute("active") final String active) {
		try {
			model.addAttribute("success", success);
	        model.addAttribute("error", error);
	        model.addAttribute("active", active);
            UserDTO user = userService.findUserById(AuthenticationUtil.getCurrentUser().getUserObj().getId()); 
            model.addAttribute("user", user);
            return "userprofile/general";
        } catch (Exception e) {
            e.printStackTrace();
            return "redirect:/";
        }
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String editAccount(@ModelAttribute("user") @Valid UserDTO user, @RequestParam("avatar") MultipartFile file, Model model, RedirectAttributes redirectAttributes) {
        UserProfileResult result = userProfileService.update(user, file);
        if (result.getStatus().equals(ResultStatus.ERROR)) {
            redirectAttributes.addFlashAttribute("error", result.getErrorList());
            redirectAttributes.addAttribute("active", "edit");
            model.addAttribute("user", user);
            return "redirect:/profile/myaccount";
        } else {
            redirectAttributes.addAttribute("success", result.getMsgList());
            return "redirect:/profile/myaccount";
        }
    }
}
