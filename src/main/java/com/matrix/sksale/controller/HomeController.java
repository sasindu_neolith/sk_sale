package com.matrix.sksale.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.matrix.sksale.security.api.SecurityContextAccessor;

@Controller
public class HomeController {

	@Autowired
	private SecurityContextAccessor securityContextAccessor;

	@RequestMapping(value = { "/session-expire-redirect"}, method = RequestMethod.GET)
	public String sessionExpiredRedirect() {
		return "/session-expire-redirect";
	}

	@RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
	public String index(Model model) {
		return "dashboard/index";
	}

	@RequestMapping(value = { "/dashboard" }, method = RequestMethod.GET)
	public String dashboard(Model model) {
		return "dashboard/index";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model) {
		if (securityContextAccessor.isCurrentAuthenticationAnonymous()) {
			return "/login";
		} else {
			return "redirect:/";
		}
	}

}
