package com.matrix.sksale.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@Import(value = { DatabaseConfiguration.class, EmailConfiguration.class })
@ComponentScan(basePackages = {"com.matrix.sksale.service", "com.matrix.sksale.util", "com.matrix.sksale.dao",
		"com.matrix.sksale.aop", "com.matrix.sksale.security", "com.matrix.sksale.validation", "com.matrix.sksale.support"})
@EnableAspectJAutoProxy
@EnableScheduling
public class SpringConfiguration {

}
