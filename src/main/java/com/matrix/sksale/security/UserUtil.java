package com.matrix.sksale.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.matrix.sksale.model.admin.User;

public class UserUtil {


    public static User getAuthenticatedLoggedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || !authentication.isAuthenticated()) {
            return null;
        }
        return ((CurrentUser) authentication.getPrincipal()).getUserObj();
    }





}
