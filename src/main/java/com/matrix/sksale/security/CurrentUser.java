package com.matrix.sksale.security;

import org.springframework.security.core.GrantedAuthority;

import com.matrix.sksale.dto.admin.UserCredentialDTO;
import com.matrix.sksale.model.admin.User;

import java.util.Collection;

public class CurrentUser extends org.springframework.security.core.userdetails.User {
    
    private static final long serialVersionUID = -2385326206773475679L;
	
	private Integer userId;
    private String userName;
    private User userObj;

    public CurrentUser(UserCredentialDTO user, Collection<GrantedAuthority> authorities, User obj) {
        super(user.getUserName(), user.getPassword(), authorities);
        this.userId = user.getId();
        this.userName = user.getUserName();
        this.userObj = obj;
    }

	public Integer getUserId() {
		return userId;
	}

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

	public User getUserObj() {
		return userObj;
	}

	public void setUserObj(User userObj) {
		this.userObj = userObj;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
