package com.matrix.sksale.security.api;

public interface SecurityContextAccessor {

    boolean isCurrentAuthenticationAnonymous();

}
