package com.matrix.sksale.dto.sale;

import java.math.BigDecimal;
import java.util.Date;

import com.matrix.sksale.dto.BaseDTO;

public class VehicleSaleDTO extends BaseDTO {

	private Integer id;
	
	private String supplierName;
	private String itemName;
	private String chassisNo;
	private String customerName;
	
	private Date date;
	private String strDate;
	private Date paymentDate;
	private Date sellingDate;
	private Date bankDepositDate;
	private Date cashDepositDate;
	private Date repairDate;
	
	private BigDecimal purchasedAmount;
	private BigDecimal taxAmount;
	private BigDecimal totalAmount;	
	private BigDecimal sellingAmount;	
	private BigDecimal bankDepositAmount;
	private BigDecimal bankCommisionAmount;	
	private BigDecimal cashDepositAmount;	
	private BigDecimal repairChargeAmount;
	private BigDecimal recycleChargeAmount;
	
	private Boolean isSold;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getChassisNo() {
		return chassisNo;
	}

	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Date getSellingDate() {
		return sellingDate;
	}

	public void setSellingDate(Date sellingDate) {
		this.sellingDate = sellingDate;
	}

	public Date getBankDepositDate() {
		return bankDepositDate;
	}

	public void setBankDepositDate(Date bankDepositDate) {
		this.bankDepositDate = bankDepositDate;
	}

	public Date getCashDepositDate() {
		return cashDepositDate;
	}

	public void setCashDepositDate(Date cashDepositDate) {
		this.cashDepositDate = cashDepositDate;
	}

	public Date getRepairDate() {
		return repairDate;
	}

	public void setRepairDate(Date repairDate) {
		this.repairDate = repairDate;
	}

	public BigDecimal getPurchasedAmount() {
		return purchasedAmount;
	}

	public void setPurchasedAmount(BigDecimal purchasedAmount) {
		this.purchasedAmount = purchasedAmount;
	}

	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getSellingAmount() {
		return sellingAmount;
	}

	public void setSellingAmount(BigDecimal sellingAmount) {
		this.sellingAmount = sellingAmount;
	}

	public BigDecimal getBankDepositAmount() {
		return bankDepositAmount;
	}

	public void setBankDepositAmount(BigDecimal bankDepositAmount) {
		this.bankDepositAmount = bankDepositAmount;
	}

	public BigDecimal getBankCommisionAmount() {
		return bankCommisionAmount;
	}

	public void setBankCommisionAmount(BigDecimal bankCommisionAmount) {
		this.bankCommisionAmount = bankCommisionAmount;
	}

	public BigDecimal getCashDepositAmount() {
		return cashDepositAmount;
	}

	public void setCashDepositAmount(BigDecimal cashDepositAmount) {
		this.cashDepositAmount = cashDepositAmount;
	}

	public BigDecimal getRepairChargeAmount() {
		return repairChargeAmount;
	}

	public void setRepairChargeAmount(BigDecimal repairChargeAmount) {
		this.repairChargeAmount = repairChargeAmount;
	}

	public BigDecimal getRecycleChargeAmount() {
		return recycleChargeAmount;
	}

	public void setRecycleChargeAmount(BigDecimal recycleChargeAmount) {
		this.recycleChargeAmount = recycleChargeAmount;
	}

	public Boolean getIsSold() {
		return isSold;
	}

	public void setIsSold(Boolean isSold) {
		this.isSold = isSold;
	}

	public String getStrDate() {
		return strDate;
	}

	public void setStrDate(String strDate) {
		this.strDate = strDate;
	}

}
