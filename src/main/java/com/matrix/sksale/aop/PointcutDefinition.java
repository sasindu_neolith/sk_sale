package com.matrix.sksale.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class PointcutDefinition {

	@Pointcut("within(com.matrix.sksale.controller..*)")
	public void webLayer() {
	}

	@Pointcut("within(com.matrix.sksale.service..*)")
	public void serviceLayer() {
	}

	@Pointcut("within(com.matrix.sksale.dao..*)")
	public void dataAccessLayer() {
	}

}
