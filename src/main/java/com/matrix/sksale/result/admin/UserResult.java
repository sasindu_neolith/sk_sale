package com.matrix.sksale.result.admin;

import com.matrix.sksale.dto.admin.UserDTO;
import com.matrix.sksale.model.admin.User;
import com.matrix.sksale.result.BaseResult;

public class UserResult extends BaseResult<User, UserDTO> {

    public UserResult(User domain, UserDTO dto) {
        super(domain, dto);
    }

    @Override
    public void updateDtoIdAndVersion() {
        getDtoEntity().setId(getDomainEntity().getId());
        getDtoEntity().setVersion(getDomainEntity().getVersion());
    }

}
