package com.matrix.sksale.result.sale;

import com.matrix.sksale.dto.sale.VehicleSaleDTO;
import com.matrix.sksale.model.sale.VehicleSale;
import com.matrix.sksale.result.BaseResult;

public class VehicleSaleResult extends BaseResult<VehicleSale, VehicleSaleDTO> {

    public VehicleSaleResult(VehicleSale domain, VehicleSaleDTO dto) {
        super(domain, dto);
    }

    @Override
    public void updateDtoIdAndVersion() {
        getDtoEntity().setId(getDomainEntity().getId());
        getDtoEntity().setVersion(getDomainEntity().getVersion());
    }

}