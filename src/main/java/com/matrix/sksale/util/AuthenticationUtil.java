package com.matrix.sksale.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.matrix.sksale.constants.UserLevel;
import com.matrix.sksale.model.admin.User;
import com.matrix.sksale.security.CurrentUser;

public class AuthenticationUtil {

	public static CurrentUser getCurrentUser() {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if (authentication == null || !authentication.isAuthenticated()|| authentication.getPrincipal().equals("anonymousUser")) {
			return null;
		}

		return (CurrentUser) authentication.getPrincipal();
	}

	public static User getAuthenticatedUser() {
		return getCurrentUser().getUserObj();
	}

	public static String getUserName() {
		if ( getCurrentUser() != null && getCurrentUser().getUserName() != null ) { 
			return getCurrentUser().getUserName();
		}
		return null;
	}

//	private static void setGeneralUserAuthority() {
//		final Set<GrantedAuthority> authorities = new HashSet<>();
//		authorities.add(new SimpleGrantedAuthority("GENERAL_LEVEL"));
//		replaceCurrentAuthorities(authorities);
//	}

	public static Boolean isAuthUserAdminLevel() {
		return getAuthenticatedUser().getUserLevel().equals(UserLevel.ADMIN_LEVEL);
	}

	public static Boolean isAuthUserGeneralLevel() {
		return getAuthenticatedUser().getUserLevel().equals(UserLevel.GENERAL_LEVEL);
	}

//	private static void replaceCurrentAuthorities(final Set<GrantedAuthority> authorities) {
//		final Object credentials = SecurityContextHolder.getContext().getAuthentication().getCredentials();
//		final Authentication auth = new UsernamePasswordAuthenticationToken(getCurrentUser(), credentials, authorities);
//		SecurityContextHolder.getContext().setAuthentication(auth);
//	}

}
