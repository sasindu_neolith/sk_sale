package com.matrix.sksale.util.search.sale;

import com.matrix.sksale.util.search.BaseSearchPropertyMapper;

public class VehicleSaleSearchPropertyMapper extends BaseSearchPropertyMapper {

	private static VehicleSaleSearchPropertyMapper instance = null;

	private VehicleSaleSearchPropertyMapper() {
	}

	public static VehicleSaleSearchPropertyMapper getInstance() {
		if (instance == null) {
			instance = new VehicleSaleSearchPropertyMapper();
		}
		return instance;
	}

	@Override
	protected void mapSearchParamsToPropertyParams(String tableColumn) {

		switch (tableColumn) {

		case "strDate" : 
			addFocusColumns("date");
			break; 
			
		default:
			break;

		}
	}


}
