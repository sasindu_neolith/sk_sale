package com.matrix.sksale.util.search;

import java.util.ArrayList;
import java.util.List; 
import org.springframework.data.jpa.datatables.mapping.Search;

import com.matrix.sksale.repository.FocusColumn;
import com.matrix.sksale.repository.FocusDataTablesInput;

public abstract class BaseSearchPropertyMapper {

	private List<FocusColumn> extraFocusColumns = new ArrayList<>();
	private FocusColumn currentFocusColumn;

	protected abstract void mapSearchParamsToPropertyParams(String column);

	/*
	 * Search mapper for Cutstomize DataTableInput 
	 * 
	 * */
	public void generateFocusDataTableInput(FocusDataTablesInput input) {

		for (FocusColumn column : input.getColumns()) {

			if (column.getSearchable() || column.getOrderable()) {
				currentFocusColumn = column;
				mapSearchParamsToPropertyParams(currentFocusColumn.getData());
			}
		}

		input.getColumns().addAll(extraFocusColumns);
		clearData();
	}	
	
	/*
	 * Add Column to Customize DataTableInput 
	 * 
	 * */
	protected void addFocusColumns(String... properties) {
		int index = 0;		
		for (String data : properties) {
			if (index == 0) {
				currentFocusColumn.setData(data);
			} else {
				extraFocusColumns.add(createFocusExtraColumn(data));
			}
			index++;
		}
	}
	
	/*
	 * Create Extra Column to Customize DataTableInput 
	 * 
	 * */
	private FocusColumn createFocusExtraColumn(String data) {
		FocusColumn col = new FocusColumn();
		col.setData(data);
		col.setSearchable(true);
		col.setOrderable(true);
		col.setName("");
		col.setSearch(getDefaultSearch());

		return col;
	}

	protected Search getDefaultSearch() {
		Search search = new Search();
		search.setRegex(false);
		search.setValue("");

		return search;
	}
	
	private void clearData() {
		extraFocusColumns.clear();
		currentFocusColumn = null;
	}

}
