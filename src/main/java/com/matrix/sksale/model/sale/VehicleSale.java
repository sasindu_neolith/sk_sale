package com.matrix.sksale.model.sale;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.matrix.sksale.model.BaseModel;

@Entity
@Table(name = "tbl_vehicle_sale")
public class VehicleSale extends BaseModel implements Serializable {
	
	private static final long serialVersionUID = -424569245049027117L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "vehicle_sale_s")
	@SequenceGenerator(name = "vehicle_sale_s", sequenceName = "vehicle_sale_s", allocationSize = 1)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "date")
	private Date date;
	
	@Column(name = "supplier_name")
	private String supplierName;
	
	@Column(name = "item_name")
	private String itemName;
	
	@Column(name = "chassis_no")
	private String chassisNo;
	
	@Column(name = "payment_date")
	private Date paymentDate;
	
	@Column(name = "purchased_amount")
	private BigDecimal purchasedAmount;
	
	@Column(name = "tax_amount")
	private BigDecimal taxAmount;
	
	@Column(name = "total_amount")
	private BigDecimal totalAmount;
	
	@Column(name = "customer_name")
	private String customerName;
	
	@Column(name = "selling_date")
	private Date sellingDate;
	
	@Column(name = "selling_amount")
	private BigDecimal sellingAmount;
	
	@Column(name = "bank_deposit_date")
	private Date bankDepositDate;
	
	@Column(name = "bank_deposit_amount")
	private BigDecimal bankDepositAmount;
	
	@Column(name = "bank_commision_amount")
	private BigDecimal bankCommisionAmount;
	
	@Column(name = "cash_deposit_date")
	private Date cashDepositDate;
	
	@Column(name = "cash_deposit_amount")
	private BigDecimal cashDepositAmount;
	
	@Column(name = "repair_date")
	private Date repairDate;
	
	@Column(name = "repair_charge_amount")
	private BigDecimal repairChargeAmount;
	
	@Column(name = "recyle_charge_amount")
	private BigDecimal recycleChargeAmount;
	
	@Column(name = "is_sold")
	private Boolean isSold;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getChassisNo() {
		return chassisNo;
	}

	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public BigDecimal getPurchasedAmount() {
		return purchasedAmount;
	}

	public void setPurchasedAmount(BigDecimal purchasedAmount) {
		this.purchasedAmount = purchasedAmount;
	}

	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Date getSellingDate() {
		return sellingDate;
	}

	public void setSellingDate(Date sellingDate) {
		this.sellingDate = sellingDate;
	}

	public BigDecimal getSellingAmount() {
		return sellingAmount;
	}

	public void setSellingAmount(BigDecimal sellingAmount) {
		this.sellingAmount = sellingAmount;
	}

	public Date getBankDepositDate() {
		return bankDepositDate;
	}

	public void setBankDepositDate(Date bankDepositDate) {
		this.bankDepositDate = bankDepositDate;
	}

	public BigDecimal getBankDepositAmount() {
		return bankDepositAmount;
	}

	public void setBankDepositAmount(BigDecimal bankDepositAmount) {
		this.bankDepositAmount = bankDepositAmount;
	}

	public BigDecimal getBankCommisionAmount() {
		return bankCommisionAmount;
	}

	public void setBankCommisionAmount(BigDecimal bankCommisionAmount) {
		this.bankCommisionAmount = bankCommisionAmount;
	}

	public Date getCashDepositDate() {
		return cashDepositDate;
	}

	public void setCashDepositDate(Date cashDepositDate) {
		this.cashDepositDate = cashDepositDate;
	}

	public BigDecimal getCashDepositAmount() {
		return cashDepositAmount;
	}

	public void setCashDepositAmount(BigDecimal cashDepositAmount) {
		this.cashDepositAmount = cashDepositAmount;
	}

	public Date getRepairDate() {
		return repairDate;
	}

	public void setRepairDate(Date repairDate) {
		this.repairDate = repairDate;
	}

	public BigDecimal getRepairChargeAmount() {
		return repairChargeAmount;
	}

	public void setRepairChargeAmount(BigDecimal repairChargeAmount) {
		this.repairChargeAmount = repairChargeAmount;
	}

	public BigDecimal getRecycleChargeAmount() {
		return recycleChargeAmount;
	}

	public void setRecycleChargeAmount(BigDecimal recycleChargeAmount) {
		this.recycleChargeAmount = recycleChargeAmount;
	}

	public Boolean getIsSold() {
		return isSold;
	}

	public void setIsSold(Boolean isSold) {
		this.isSold = isSold;
	}

}
