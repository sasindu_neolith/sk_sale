package com.matrix.sksale.model.api;

import com.matrix.sksale.model.BaseModel;

/**
 * Defines the RootAware. 
 */
public interface RootAware<T extends BaseModel>{
	
	/**
	 * Resolve the root entity for any Child Entity.. 
	 */
	T root();
}
