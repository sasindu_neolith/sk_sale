package com.matrix.sksale.model.admin;

import javax.persistence.*;

import com.matrix.sksale.constants.UserLevel;
import com.matrix.sksale.model.BaseModel;

import java.io.Serializable;

@Entity
@Table(name="tbl_user")
public class User extends BaseModel implements Serializable {

	private static final long serialVersionUID = -819248302938019714L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "user_s")
	@SequenceGenerator(name = "user_s", sequenceName = "user_s", allocationSize = 1)
	@Column(name = "id")
	private Integer id;

	@Column(name = "full_Name")
	private String fullName;

	@Column(name = "address")
	private String address;

	@Column(name = "image_path")
	private String imagePath;

	@Column(name = "email_address")
	private String emailAddress;

	@Column(name = "personal_code")
	private String personalCode;

	@Column(name = "notes")
	private String notes;

	@Column(name = "telephone_1")
	private String telephone1;

	@Column(name = "telephone_2")
	private String telephone2;

	@Column(name = "user_title")
	private String userTitle;

	@Column(name = "sys_code")
	private Integer sysCode;

	@Column(name = "is_active")
	private Boolean isActive;
	
	@Column(name = "user_level_id")
	private UserLevel userLevel;

	@OneToOne(mappedBy = "user", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
	private UserCredential userCredential;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPersonalCode() {
		return personalCode;
	}

	public void setPersonalCode(String personalCode) {
		this.personalCode = personalCode;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getTelephone1() {
		return telephone1;
	}

	public void setTelephone1(String telephone1) {
		this.telephone1 = telephone1;
	}

	public String getTelephone2() {
		return telephone2;
	}

	public void setTelephone2(String telephone2) {
		this.telephone2 = telephone2;
	}

	public String getUserTitle() {
		return userTitle;
	}

	public void setUserTitle(String userTitle) {
		this.userTitle = userTitle;
	}

	public Integer getSysCode() {
		return sysCode;
	}

	public void setSysCode(Integer sysCode) {
		this.sysCode = sysCode;
	}

	public Boolean getActive() {
		return isActive;
	}

	public void setActive(Boolean active) {
		isActive = active;
	}

	public UserCredential getUserCredential() {
		return userCredential;
	}

	public void setUserCredential(UserCredential userCredential) {
		this.userCredential = userCredential;
	}

	public void setUserCredentials(UserCredential userCredentials) {
		userCredential = userCredentials;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public UserLevel getUserLevel() {
		return userLevel;
	}

	public void setUserLevel(UserLevel userLevel) {
		this.userLevel = userLevel;
	}

}
