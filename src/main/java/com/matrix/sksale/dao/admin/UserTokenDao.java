package com.matrix.sksale.dao.admin;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.matrix.sksale.model.admin.UserToken;
import com.matrix.sksale.repository.FocusDataTableRepository;

@Repository
public interface UserTokenDao extends FocusDataTableRepository<UserToken, Integer>{

	UserToken findById(Integer id);

    @Query("from UserToken where resetPasswordToken=:token")
    UserToken findByToken(@Param("token") String token);

}
