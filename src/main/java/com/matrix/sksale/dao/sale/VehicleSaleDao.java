package com.matrix.sksale.dao.sale;

import org.springframework.stereotype.Repository;

import com.matrix.sksale.model.sale.VehicleSale;
import com.matrix.sksale.repository.FocusDataTableRepository;

@Repository
public interface VehicleSaleDao extends FocusDataTableRepository<VehicleSale, Integer> {

}
